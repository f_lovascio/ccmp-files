#!/bin/bash
echo "installing Atomeye"
if [[ $ATOMEYE_INSTALL == 1 ]]; then
  echo "Error Atomeye is already installed"
  echo "Clean up your bash_profile and set the ATOMEYE_INSTALL variable to 0 to reinstall"
else
  echo "making binary directory"
  mkdir -p ~/.atomeye/
  echo "..."
  cp ./atomeye ~/.atomeye/.A
  cp ./run_atomeye ~/.atomeye/atomeye

  echo "making ffmpeg directory"
  mkdir -p ~/.ffmpeg/
  echo "..."
  cp ./ffmpeg ~/.atomeye/ffmpeg

  echo "making Atomeye executable"
  echo "..."
  chmod +x ~/.atomeye/atomeye
  echo "modifying bash_profile"
  echo "..."
  cat .AtomeyeProfile >> ~/.bash_profile
  source ~/.bash_profile
  echo "all done!"
fi

