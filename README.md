# CCMP Files

Files and tech instructions for the Computational Condensed Matter module SPA6315

To get the files from the repository use:

```$ git clone <https://address.of.the.directory>```

Remember to edit a copy of the files and not the originals!


## Getting started

To use dl_poly on the students3 machine, you will have to load its module. This can be done by running 

```$ module load dl_poly```

You may want to add this line to your *~/.bash_profile* as to load the dlpoly module on login.

## AtomEye
<img src="http://li.mit.edu/Archive/Graphics/A/Gallery/DNA/DNA2.jpg" width=200> <img src="http://li.mit.edu/Archive/Graphics/A/Gallery/Nanotube/Nanotube_tip.jpg" width=200> <img src="http://li.mit.edu/Archive/Graphics/A/Gallery/DNA/DNA1.jpg" width=200>

AtomEye is a rendering package for molecular dynamics simulations, a better description can be found [on their site](http://li.mit.edu/Archive/Graphics/A/). We will be using it to visualise radiation damage in crystals, but to do this you will first need to get the package up and running on your accounts. 

### Installing AtomEye
#### New way: install_atomeye script
Upon login navigate to the atomeye directory and run

```$ bash install_atomeye.sh```

then you should be good to go. You should only need to run this the first time you need to run atomeye, from then on you will be able to call atomeye in the terminal by just calling

```$ atomeye <foo>.cfg```

#### The old way (Deprecated)

**If the first way does not work, you may try this instead, but try to first call a demonstrator anyway.**

First run 

```$ source atomeye_rc```

then to make atomeye executable run 

```$ chmod +x atomeye ```

you can then use atomeye as:

```$ ./atomeye foo.cfg```

### Using AtomEye
To use AtomEye you will need X11 to be enabled in your session, in moba this is acchieved by using the **-X** flag in ssh when logging in

```$ ssh -X user@server```

If you are working on macOS/OSX you will need Xquartz for this to work!

As mentioned before AtomEye can be run by calling it on a *.cfg* file, 

```$ atomeye <foo>.cfg```

#### Making animations

If AtomEye is called on the first of a sequence of ordered *.cfg* files like *st0.cfg*, *st1.cfg* , *...*, *st9.cfg*,  with the command,

```$ atomeye st0.cfg```

the usual window will appear. By hitting the **y** key on your keyboard you can then set off AtomEye to render the rest of the sequence of *cfg*s. Once you have done this a folder called *./Jpg* will have been generated. *ffmpeg* can then be used to composite these into a video.

```$ ffmpeg -framerate 15 -i %05d.jpg <video_title>.mp4```

You will be able to produce cool animations of your simulations!

![](./gif.gif)
